#!/usr/bin/env bash
set -o xtrace
set -e

export DEBIAN_FRONTEND=noninteractive

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

export NOME_INFRA="gppd-hpc"
export CONTROLADOR_IP="192.168.30.2"
export CONTROLADOR_DNS="controlador"
export CONTROLADOR_LDAP="dc=${CONTROLADOR_DNS}"
export ARQUIVO_SENHA="${SCRIPTPATH}/confs/pass"
export REDE_INTERNA="192.168.30.2/24"
export SCRIPTPATH="${SCRIPTPATH:-$HOME/mc-hpc-share/}"
export ADMIN_USER="parque"

sudo -E apt install -y auditd
cat $SCRIPTPATH/confs/audit.rules | \
   sudo tee -a /etc/audit/rules.d/audit.rules
sudo systemctl restart auditd
sudo systemctl stop systemd-journald-audit.socket 
sudo systemctl disable systemd-journald-audit.socket
sudo systemctl mask systemd-journald-audit.socket

sudo -E apt install -y libnss-ldapd libpam-ldapd nscd nslcd nslcd-utils expect
unset DEBIAN_FRONTEND
chmod +x $SCRIPTPATH/configure_nslcd.sh
sudo -E /usr/bin/expect $SCRIPTPATH/configure_nslcd.sh
export DEBIAN_FRONTEND=noninteractive
sudo cp $ARQUIVO_SENHA /etc/infra_access
sudo chown root:root /etc/infra_access
sudo chmod 700 /etc/infra_access
sudo cp $SCRIPTPATH/authp /usr/bin/authp
sudo -E sed -i "s/CONTROLADOR_IP/${CONTROLADOR_IP}/g" /usr/bin/authp
sudo -E sed -i "s/CONTROLADOR_LDAP/${CONTROLADOR_LDAP}/g" /usr/bin/authp
sudo chown root:root /usr/bin/authp
sudo chmod 700 /usr/bin/authp
sudo cp $SCRIPTPATH/confs/custom_sshd.conf /etc/ssh/sshd_config.d/
sudo systemctl restart sshd
sudo -E sed -i  "s/# auth       required   pam_wheel.so/auth       required   pam_wheel.so group=$ADMIN_USER/" /etc/pam.d/su

sudo apt-get install -y nfs-common 
echo "$CONTROLADOR_DNS:/home       /home      nfs auto,nofail,noatime 0 0" | sudo tee -a /etc/fstab
sudo mount -a

sudo mkdir -p /scratch
sudo chmod 755 /scratch
sudo mkdir /scratch/$ADMIN_USER
sudo chown $ADMIN_USER:$ADMIN_USER /scratch/$ADMIN_USER
sudo chmod 700 /scratch/$ADMIN_USER
echo 'SCRATCH=/scratch/$(whoami)/' | sudo tee -a /etc/profile
echo 'export SCRATCH' | sudo tee -a /etc/profile

sudo cp  $SCRIPTPATH/scratch.sh /usr/bin/scratch.sh
sudo chmod 755 /usr/bin/scratch.sh
echo "session required    pam_exec.so /usr/bin/scratch.sh" | \
     sudo tee -a /etc/pam.d/common-session

sudo apt install -y build-essential pkg-config libpam0g-dev \
    libmunge-dev munge libopenmpi-dev libnuma-dev libjson-c-dev \
    libyaml-dev hwloc libhwloc-dev libcurl4-openssl-dev libdbus-1-dev \
    liblz4-dev libhttp-parser-dev libreadline-dev
cd /scratch/$ADMIN_USER/
wget https://download.schedmd.com/slurm/slurm-23.02.0.tar.bz2
mkdir slurm
tar --bzip -x -f slurm*tar.bz2 -C ./slurm --strip-components=1
cd slurm
./configure
make -j
sudo make install
sudo cp /home/munge.key /etc/munge/munge.key
sudo chown munge:munge /etc/munge/munge.key
sudo chmod 600 /etc/munge/munge.key
sudo systemctl restart munge
pushd contribs/pam_slurm_adopt/
make -j
sudo make install
popd
echo "/usr/local/lib/" | sudo tee -a /etc/ld.so.conf.d/slurm.conf
sudo ldconfig

sudo ln -s /home/slurm.conf /usr/local/etc/slurm.conf
sudo ln -s /home/cgroup.conf /usr/local/etc/cgroup.conf
sudo cp $SCRIPTPATH/confs/gres.conf /usr/local/etc/gres.conf
sudo chown slurm:root /usr/local/etc/gres.conf
sudo chmod 644 /usr/local/etc/gres.conf

sudo cp etc/slurmd.service /etc/systemd/system/
sudo mkdir /var/spool/slurmd
sudo chown slurm /var/spool/slurmd
sudo mkdir /etc/systemd/system/slurmd.service.d/
sudo cp $SCRIPTPATH/confs/slurmd_override.conf /etc/systemd/system/slurmd.service.d/override.conf
sudo systemctl daemon-reload
sudo systemctl start slurmd
sudo systemctl enable slurmd

sudo patch /etc/pam.d/sshd $SCRIPTPATH/confs/diff_pam_sshd_perm 
sudo sed -i "s/ADMIN_USER/${ADMIN_USER}/g" /etc/pam.d/sshd
echo -e "+ : $ADMIN_USER : ALL\n- : ALL : ALL" | sudo tee -a /etc/security/access.conf
sudo systemctl restart sshd
