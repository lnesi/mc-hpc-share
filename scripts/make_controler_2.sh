#!/usr/bin/env bash
set -o xtrace
set -e

export DEBIAN_FRONTEND=noninteractive

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

export NOME_INFRA="gppd-hpc"
export CONTROLADOR_IP="192.168.30.2"
export CONTROLADOR_DNS="controlador"
export CONTROLADOR_LDAP="dc=${CONTROLADOR_DNS}"
export ARQUIVO_SENHA="${SCRIPTPATH}/confs/pass"
export REDE_INTERNA="192.168.30.2/24"
export SCRIPTPATH="${SCRIPTPATH:-$HOME/mc-hpc-share/}"
export ADMIN_USER="parque"

sudo apt-get install -y nfs-kernel-server

echo "/home       $REDE_INTERNA(rw,sync,no_root_squash,no_subtree_check)" | sudo tee -a /etc/exports

sudo systemctl restart nfs-kernel-server

sudo apt install -y build-essential pkg-config libpam0g-dev \
     libmunge-dev munge mariadb-server libopenmpi-dev libmariadb-dev \
     libnuma-dev libjson-c-dev libyaml-dev hwloc libhwloc-dev \
     libcurl4-openssl-dev libdbus-1-dev liblz4-dev \
     libhttp-parser-dev libreadline-dev

sudo sh -c 'dd if=/dev/urandom bs=1 count=1024 > /etc/munge/munge.key'
sudo chown munge:munge /etc/munge/munge.key
sudo chmod 600 /etc/munge/munge.key
sudo cp /etc/munge/munge.key /home/munge.key
sudo chown root:root /home/munge.key
sudo chmod 600 /home/munge.key

sudo systemctl restart munge

wget https://download.schedmd.com/slurm/slurm-23.02.0.tar.bz2
mkdir slurm
tar --bzip -x -f slurm*tar.bz2 -C ./slurm --strip-components=1

cd slurm
./configure
make -j
sudo make install

read -p "Crie o usuario slurm via phpldapadmin, então pressione enter para continuar" answer

echo -n 'AuthType=auth/munge
DbdHost=localhost
DbdPort=6819
LogFile=/usr/local/etc/dbd.log
SlurmUser=slurm
StorageHost=localhost
StorageLoc=slurm_db
StoragePass=' | sudo tee /usr/local/etc/slurmdbd.conf
sudo sh -c "cat $SCRIPTPATH/confs/pass >> /usr/local/etc/slurmdbd.conf"
sudo sh -c 'echo "" >> /usr/local/etc/slurmdbd.conf'
echo 'StoragePort=3306
StorageType=accounting_storage/mysql
StorageUser=slurm
DebugLevel=info' | sudo tee -a /usr/local/etc/slurmdbd.conf

sudo chown slurm:root /usr/local/etc/
sudo chown slurm:root /usr/local/etc/slurmdbd.conf
sudo chmod 600 /usr/local/etc/slurmdbd.conf

echo -n "CREATE DATABASE slurm_db;
CREATE USER 'slurm'@localhost IDENTIFIED BY '" > /tmp/mysql_query
cat $SCRIPTPATH/confs/pass >> /tmp/mysql_query
echo "';
GRANT ALL PRIVILEGES ON slurm_db.* TO 'slurm'@localhost;" >> /tmp/mysql_query
sudo mysql -u root < /tmp/mysql_query

echo "[mysqld]
innodb_buffer_pool_size=4096M
innodb_log_file_size=64M
innodb_lock_wait_timeout=900" | sudo tee -a /etc/mysql/conf.d/mysql.cnf
sudo systemctl restart mariadb.service

sudo cp etc/slurmdbd.service /etc/systemd/system/
sudo systemctl start slurmdbd
sudo systemctl enable slurmdbd
sudo systemctl status slurmdbd

sudo cp $SCRIPTPATH/confs/slurm.conf /home/slurm.conf
sudo sed -i "s/CONTROLADOR_DNS/${CONTROLADOR_DNS}/g" /home/slurm.conf
sudo sed -i "s/CONTROLADOR_IP/${CONTROLADOR_IP}/g" /home/slurm.conf
sudo sed -i "s/NOME_INFRA/${NOME_INFRA}/g" /home/slurm.conf
sudo cp $SCRIPTPATH/confs/cgroup.conf /home/cgroup.conf

sudo chown slurm:root /home/slurm.conf
sudo chmod 644 /home/slurm.conf
sudo ln -s /home/slurm.conf /usr/local/etc/slurm.conf
sudo chown slurm:root /home/cgroup.conf
sudo chmod 644 /home/cgroup.conf
sudo ln -s /home/cgroup.conf /usr/local/etc/cgroup.conf
sudo cp etc/slurmctld.service /etc/systemd/system/
sudo mkdir /var/spool/slurm.state
sudo chown slurm /var/spool/slurm.state

sudo mkdir /etc/systemd/system/slurmctld.service.d/
sudo cp $SCRIPTPATH/confs/slurmctld_override.conf /etc/systemd/system/slurmctld.service.d/override.conf
sudo systemctl daemon-reload

sudo systemctl start slurmctld
sudo systemctl enable slurmctld
sudo systemctl restart slurmctld

sudo sacctmgr -i create account $NOME_INFRA
sudo sacctmgr -i add user $ADMIN_USER account=$NOME_INFRA
