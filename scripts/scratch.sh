#!/bin/bash
user=$PAM_USER
if [ ! -d /scratch/$user ]; then
   mkdir /scratch/$user
   chown $user:members /scratch/$user
   chmod 700 /scratch/$user
fi
