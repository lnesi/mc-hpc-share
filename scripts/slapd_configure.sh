#!/usr/bin/expect
spawn dpkg-reconfigure slapd -freadline
expect "Omit OpenLDAP server configuration?"
send "no\r"

expect "DNS domain name:"
send "$env(CONTROLADOR_DNS)\r"

expect "Organization name:"
send "$env(NOME_INFRA)\r"

expect "Administrator password:"
send "[read [open "$env(ARQUIVO_SENHA)" r]]\r"

expect "Confirm password:"
send "[read [open "$env(ARQUIVO_SENHA)" r]]\r"

expect "Do you want the database to be removed when slapd is purged?"
send "no\r"

expect "Move old database?"
send "yes\r"
expect eof
