#!/usr/bin/expect
spawn dpkg-reconfigure libnss-ldapd -freadline
expect "Name services to configure:"
send "1 2 3\r"
expect eof

spawn dpkg-reconfigure nslcd -freadline
expect "LDAP server URI:"
send "ldap://$env(CONTROLADOR_IP)/\r"

expect "LDAP server search base:"
send "$env(CONTROLADOR_LDAP)\r"

expect "LDAP authentication to use:"
send "2\r"

expect "LDAP database user:"
send "cn=admin,$env(CONTROLADOR_LDAP)\r"

expect "LDAP user password:"
send "[read [open "$env(ARQUIVO_SENHA)" r]]\r"

expect "Use StartTLS?"
send "no\r"
expect eof
