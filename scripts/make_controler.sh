#!/usr/bin/env bash
set -o xtrace
set -e

export DEBIAN_FRONTEND=noninteractive

SCRIPT=$(realpath "$0")
SCRIPTPATH=$(dirname "$SCRIPT")

export NOME_INFRA="gppd-hpc"
export CONTROLADOR_IP="192.168.30.2"
export CONTROLADOR_DNS="controlador"
export CONTROLADOR_LDAP="dc=${CONTROLADOR_DNS}"
export ARQUIVO_SENHA="${SCRIPTPATH}/confs/pass"
export REDE_INTERNA="192.168.30.2/24"
export SCRIPTPATH="${SCRIPTPATH:-$HOME/mc-hpc-share/}"
export ADMIN_USER="parque"

sudo apt install -y ufw
sudo ufw default deny incoming
sudo ufw allow 22
sudo ufw allow 80
sudo ufw allow from $REDE_INTERNA to any
sudo ufw --force enable

sudo apt install -y rsyslog fail2ban
sudo cp $SCRIPTPATH/confs/jail.local /etc/fail2ban/jail.local
sudo chown root:root /etc/fail2ban/jail.local
sudo chmod 644 /etc/fail2ban/jail.local
sudo service fail2ban restart

sudo -E apt install -y auditd
cat $SCRIPTPATH/confs/audit.rules | \
   sudo tee -a /etc/audit/rules.d/audit.rules
sudo systemctl restart auditd
sudo systemctl stop systemd-journald-audit.socket 
sudo systemctl disable systemd-journald-audit.socket
sudo systemctl mask systemd-journald-audit.socket

sudo -E apt install -y slapd ldap-utils

sudo apt install -y expect patch

unset DEBIAN_FRONTEND
chmod +x $SCRIPTPATH/slapd_configure.sh
sudo -E /usr/bin/expect $SCRIPTPATH/slapd_configure.sh

export DEBIAN_FRONTEND=noninteractive

sudo ldapadd -Y EXTERNAL -H ldapi:/// -f $SCRIPTPATH/confs/ldap_anon.ldif

sudo ldapadd -Y EXTERNAL -H ldapi:/// -f $SCRIPTPATH/confs/openssh-lpk.ldif

sudo apt install -y phpldapadmin

sudo patch -l /etc/phpldapadmin/config.php \
          $SCRIPTPATH/confs/diff_pla_config
sudo sed -i "s/NOME_INFRA/${NOME_INFRA}/g" /etc/phpldapadmin/config.php
sudo sed -i "s/CONTROLADOR_LDAP/${CONTROLADOR_LDAP}/g" \
          /etc/phpldapadmin/config.php

sudo patch -l /etc/phpldapadmin/templates/creation/posixAccount.xml \
           $SCRIPTPATH/confs/diff_pla_posix
sudo patch -l /usr/share/phpldapadmin/htdocs/create_confirm.php \
           $SCRIPTPATH/confs/diff_pla_create
sudo patch -l /usr/share/phpldapadmin/lib/ds_ldap_pla.php \
           $SCRIPTPATH/confs/diff_pla_pla
sudo patch -l /usr/share/phpldapadmin/lib/PageRender.php \
           $SCRIPTPATH/confs/diff_pla_page

echo "session required    pam_mkhomedir.so skel=/etc/skel umask=0077" | sudo tee -a /etc/pam.d/common-session

sudo patch -l /etc/skel/.bashrc $SCRIPTPATH/confs/diff_skel_bashrc

sudo -E apt install -y libnss-ldapd libpam-ldapd nscd nslcd nslcd-utils expect
unset DEBIAN_FRONTEND
chmod +x $SCRIPTPATH/configure_nslcd.sh
sudo -E /usr/bin/expect $SCRIPTPATH/configure_nslcd.sh
export DEBIAN_FRONTEND=noninteractive
sudo cp $ARQUIVO_SENHA /etc/infra_access
sudo chown root:root /etc/infra_access
sudo chmod 700 /etc/infra_access
sudo cp $SCRIPTPATH/authp /usr/bin/authp
sudo -E sed -i "s/CONTROLADOR_IP/${CONTROLADOR_IP}/g" /usr/bin/authp
sudo -E sed -i "s/CONTROLADOR_LDAP/${CONTROLADOR_LDAP}/g" /usr/bin/authp
sudo chown root:root /usr/bin/authp
sudo chmod 700 /usr/bin/authp
sudo cp $SCRIPTPATH/confs/custom_sshd.conf /etc/ssh/sshd_config.d/
sudo systemctl restart sshd
sudo -E sed -i  "s/# auth       required   pam_wheel.so/auth       required   pam_wheel.so group=$ADMIN_USER/" /etc/pam.d/su
